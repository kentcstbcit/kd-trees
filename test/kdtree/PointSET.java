import  edu.princeton.cs.algs4.*;

public class PointSET {
  private SET<Point2D> pset;

  public         PointSET(){                               // construct an empty set of points 
    pset = new SET<Point2D>();

  }
  public           boolean isEmpty(){                      // is the set empty? 
    return pset.size() == 0;
  }
    public               int size() {                         // number of points in the set 
      return pset.size();
    }
    public              void insert(Point2D p){              // add the point to the set (if it is not already in the set)

      if (p == null) throw new IllegalArgumentException("called insert() with a null key");
      pset.add(p);
    }
    public           boolean contains(Point2D p){            // does the set contain point p? 
      return pset.contains(p);
    }


    // draw all points to standard draw 
    public              void draw(){
      
      for(Point2D one: pset)
        one.draw();

    }      


    // all points that are inside the rectangle (or on the boundary) 
    public Iterable<Point2D> range(RectHV rect){      

      if (rect == null) throw new IllegalArgumentException("called range() with a null rect");
      SET<Point2D> inr = new SET<Point2D>();

      for(Point2D one: pset) {
        if(rect.contains(one))
          inr.add(one);
      }

      return inr;

     /* private class riterable implements Iterable<Point2D> {
        public Iterable<Point2D> iterator() {
          return new Iterator<Point2D>() {

            private 

            public riterable(){


            }

            public boolean hasNext(){
            }

            public Point2D next(){
            }
            
            public void remove(){
            }

          }
        }
      }*/

    }


    // a nearest neighbor in the set to point p; null if the set is empty 
    public           Point2D nearest(Point2D p){            

      if (p == null) throw new IllegalArgumentException("called nearest() with a null p");
      if(pset.isEmpty()) return null;
      Point2D minp = pset.min();
      double mind = p.distanceTo(pset.min());
      for(Point2D one: pset) {
        double tmp = p.distanceTo(one);
        if(tmp < mind){
          minp = one;
          mind = tmp;
        }


      }

      return minp;

    }

 // unit testing of the methods (optional) 
//    public static void main(String[] args)             
}
