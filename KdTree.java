import  edu.princeton.cs.algs4.*;
import java.util.*;

public class KdTree {

  private Node root;
  private static double nearcached = 2;

  private class Node {
    private Point2D p;
    private int depth;
    private int size; 
    private Node left,right;

    public Node(Point2D p, int size,int d) {
      this.p = p;
      this.depth = d;
      this.size = size;
    }

    public boolean iscompareX() {
      return depth%2 == 0;
    }



  }
  

  // construct an empty set of points 
  public         KdTree() {
  } 
  // is the set empty? 

  public           boolean isEmpty() {
    return root == null;
  }    

  // number of points in the set 
  public               int size() {    
    return size(root);    
  }

  private int size(Node n) {
    if(n == null) return 0;
    return n.size;
  }


  // add the point to the set (if it is not already in the set)
  public              void insert(Point2D p){              
    if(p == null) throw new IllegalArgumentException("insert(Point2D) with a null key");
    
    root = put(root, p, 0);
  }
  
  private Node put(Node x, Point2D p, int deep) {
    if(x == null) return new Node(p,1,deep);
    if(x.p.equals(p)) return x;

    double cmp;
    //compare x
    if(x.depth%2 == 0) {
      cmp = p.x() - x.p.x();
    } else {
      //compare y
      cmp = p.y() - x.p.y();
    }
    if(cmp < 0) 
      x.left  = put(x.left,p, deep + 1); 
    else if(cmp >0)
      x.right = put(x.right,p,deep + 1);
    else                  //if same compared value, consider it same level
      x.right = put(x.right,p , deep);

    x.size = 1 + size(x.left) + size(x.right);
    return x;
  }

  private boolean contains(Point2D tar, Node acc) {
    if(tar == null) throw new IllegalArgumentException("get() with null");
    if(acc == null) return false;
    if(tar.equals(acc.p)) return true;

    double tmp;
    if(acc.iscompareX()) {
      tmp = tar.x() - acc.p.x();
    } else {
      tmp = tar.y() - acc.p.y();
    }

    if(tmp < 0) return contains(tar, acc.left);
    else return contains(tar,acc.right);
  }

  // does the set contain point p? 
  public           boolean contains(Point2D p){ 
    return contains(p,root);
  }

  private void draw(Node start) {
    if(start == null) return;
    else {
      start.p.draw();
      draw(start.left);
      draw(start.right);
    }
  }

  // draw all points to standard draw 
  public              void draw(){
    draw(root);
    drawline(root,0,1,0,1);
  }

  private void drawline(Node n,double xmin,double xmax,double ymin,double ymax) {
    if(n == null) return;
    if(n.iscompareX()) {
      StdDraw.setPenColor(StdDraw.RED);
      StdDraw.line(n.p.x(),ymin,n.p.x(),ymax);
      drawline(n.left,xmin,n.p.x(),ymin,ymax);
      drawline(n.right,n.p.x(), xmax, ymin,ymax);
    } else {
      StdDraw.setPenColor(StdDraw.BLUE);
      StdDraw.line(xmin, n.p.y(), xmax , n.p.y());
      drawline(n.left,xmin, xmax ,ymin, n.p.y() );
      drawline(n.right,xmin, xmax,  n.p.y() ,ymax);
    }

  }

  private void range(RectHV rect, Node mid, ResizingArrayQueue<Point2D> acc) {
    if(mid == null) return;
    if(rect.contains(mid.p)) acc.enqueue(mid.p);

    if(mid.iscompareX()) {
      if(rect.xmax() < mid.p.x()) 
        range(rect, mid.left, acc);
      else if(rect.xmin() > mid.p.x())
        range(rect, mid.right, acc);
      else {
        range(rect, mid.left, acc);
        range(rect, mid.right, acc);
      }
    } else {
      if(rect.ymax() < mid.p.y()) 
        range(rect, mid.left, acc);
      else if(rect.ymin() > mid.p.y())
        range(rect, mid.right, acc);
      else {
        range(rect, mid.left, acc);
        range(rect, mid.right, acc);
      }
    }

  }


  public Iterable<Point2D> range(RectHV rect){             // all points that are inside the rectangle (or on the boundary) 

    if(rect == null) throw new IllegalArgumentException("range() with null");
    ResizingArrayQueue<Point2D> inr = new ResizingArrayQueue<Point2D>();
    range(rect, root, inr);

    return inr;



  }

  private Node nearest(Point2D p, Node n, Node near, double x1,double y1,double x2,double y2, double nx1,double ny1,double nx2,double ny2,double tsnx1,double tsny1,double tsnx2,double tsny2) {
    if(n == null) return near;
    double tmp = p.distanceTo(n.p); 
    if(tmp < nearcached) {near = n;nearcached = tmp;}
    // same as the query point
    if(p.equals(n.p)) return n;
    Node next,other;
    double otherdis; 
    RectHV otherRe;
  /*   nx1 = x1;
    nx2 = x2;
    ny1 = y1;
    ny2 = y2;

    tsnx1 = x1;
    tsnx2 = x2;
    tsny1 = y1;
    tsny2 = y2;
*/
    if(n.iscompareX()) {
      if(p.x() < n.p.x()) {
        nx1 = n.p.x();
        tsnx2 = nx1;
        next = n.left;
        other = n.right;
      } else {
        nx2 = n.p.x();
        tsnx1 = nx2;
        next = n.right;
        other = n.left;
      }

    } else {
      if(p.y() < n.p.y()) {
        ny1 = n.p.y();
        tsny2 = ny1;
        next = n.left;
        other = n.right;
      } else {
        ny2 = n.p.y();
        tsny1 = ny2;
        next = n.right;
        other = n.left;
      }
    }
    
    //check whether to prune the other subtree
    near =  nearest(p, next, near, tsnx1,tsny1,tsnx2,tsny2, tsnx1,tsny1,tsnx2,tsny2, tsnx1,tsny1,tsnx2,tsny2);
    //both sides had checked


    otherRe = new RectHV(nx1,ny1,nx2,ny2);
    otherdis = otherRe.distanceTo(p);
    if(otherdis < nearcached && other != null) {
      near = nearest(p,other,near, nx1,ny1,nx2,ny2,nx1,ny1,nx2,ny2,nx1,ny1,nx2,ny2);      
    }

    return near;

      

  }

  public           Point2D nearest(Point2D p){             // a nearest neighbor in the set to point p; null if the set is empty 
    if(p == null) throw new IllegalArgumentException("nearest() with null");
    if(isEmpty()) return null;
    Node ne =  nearest(p, root, root, 0,0,1,1,0,0,1,1,0,0,1,1);   
    nearcached = 2;
    if(ne  == null) return null;
    return ne.p;
  }

  

  public static void main(String[] args){                  // unit testing of the methods (optional) 

  }
}
